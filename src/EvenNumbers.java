

public class EvenNumbers {

	
	/*  
	 * A function that checks if a number is even
	 * 	isEven(number)
	 * Req 1:    If number is even, return TRUE
	 * Req 2:    If number is odd, return FALSE
	 */
	
	public boolean isEven(int number) {
		// REFACTOR --> improve read-ability of code
		// by changing the parameter name from n to number
		if (number % 2 == 0) {
			return true;
		}
		return false;
		
	}
	
	
	
}
