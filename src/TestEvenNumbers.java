import static org.junit.Assert.*;

import org.junit.Test;

public class TestEvenNumbers {
	/*  
	 * A function that checks if a number is even
	 * 	isEven(number)
	 * Req 1:    If number is even, return TRUE
	 * Req 2:    If number is odd, return FALSE
	 */
	
	/* PROCESS FOR R/G/REFACTOR
	 * 1.  Write a FAILING test case
	 * 		--> test case with a lot of errors (does not compile)
	 * 		--> test case with red lines 
	 * 		--> test case that is fails when you run the JUnit test
	 * 
	 * 2.  Write the MINIMUM amount to code FIX the test case
	 * 		--> Fix the red lines
	 * 		--> Make test case pass
	 * 
	 * 3.  Refactor your code
	 */
	
	@Test
	public void testInputIsEven() {
		EvenNumbers abc = new EvenNumbers();
		boolean result = abc.isEven(80);
		assertEquals(true, result);
	}
	
	@Test
	public void testInputIsOdd() {
		// REQ 2 - try to break this test case
		// by changing the input
		EvenNumbers abc = new EvenNumbers(); 
		boolean result = abc.isEven(35);
		assertEquals(false, result);
	}

}
